# docker-graylog

## Usage

1. Run Docker in Swarm mode
    - `docker swarm init`
2. Create an overlay network for RabbitMQ
    - `docker network create --driver overlay --attachable rabbitmq`
3. Start a RabbitMQ service in its own overlay network
    - `docker service create --name rabbitmq --publish mode=host,target=5672,published=5672 --publish mode=host,target=15672,published=15672 --network rabbitmq --hostname rabbitmq --restart-condition on-failure  registry.gitlab.com/sadesyllas/rabbitmq-mng-chx:1.0.0`
4. Deploy Graylog's stack (MongoDB, ElasticSearch and Gralog) and
  configure it to also use RabbitMQ's overlay network
    - `docker stack deploy -c graylog.yml g`
5. Remove Graylog's stack
    - `docker stack rm g`
